# Asteroid


## Описание
В будущем, человечество столкнулось с угрозой внеземных захватчиков, которые начали атаковать Землю. Чтобы бороться с этой угрозой, была разработана новейшая технология - симуляция тренировки солдат.

Игрок берет на себя роль командира отряда солдат, которых необходимо подготовить к сражениям с вражескими силами. Основной инструмент тренировки - симулятор, который создает виртуальное пространство, наполненное астероидами и другими опасностями.

## Игровой процесс

![](https://sun9-57.userapi.com/impg/QvZMVS8_UNTVHPq7npprY-hOQWfmFNBR6LPK4Q/QRcqsUJTSAA.jpg?size=1280x722&quality=95&sign=7ce0b5b88a8d750db69fcd934e88e9a2&c_uniq_tag=RHI0T52_bYDvdiSpVb1Pr4mt0F-LCMTSbyfiPcQy9ec&type=album)
![](https://sun9-73.userapi.com/impg/EDG1zy4MBZ-kiwCS7K-SVsHv6rjQH5YS9YFUxg/CSTlprz7Xuk.jpg?size=1280x714&quality=95&sign=ebe619c4d5d1f80e8577cb32d4ebee24&c_uniq_tag=g2SKHrRdJfONV2cce5TClBFmrfMDEBSsX2HVKWbSm8I&type=album)
