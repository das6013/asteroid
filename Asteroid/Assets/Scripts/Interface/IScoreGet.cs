using Player;

public interface IScoreGet 
{
    public GameManagerModel GameManagerModel{ get; }
    public int Score 
    {
        get;set;
    }

    public void GetScore();
}
