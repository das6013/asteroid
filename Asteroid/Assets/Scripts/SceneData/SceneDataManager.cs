using UnityEngine;

public class SceneDataManager : MonoBehaviour
{
    [SerializeField] private Bullet _bulletPrefab;
    [SerializeField] private UFOModel _ufoPrefab;
    [SerializeField] private Canvas _mainPlace;

    private void Awake()
    {
        _bulletPrefab.PlaneDistance = _mainPlace.transform.position.z;
        _ufoPrefab.PlaneDistance= _mainPlace.transform.position.z;
    }
}
