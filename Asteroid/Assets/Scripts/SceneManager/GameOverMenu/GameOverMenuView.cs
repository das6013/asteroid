using System;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenuView : MonoBehaviour
{
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _restartButton;
    public event EventHandler onExitGame;
    public event EventHandler onRestartGame;

    private void OnEnable()
    {
        _exitButton.onClick.AddListener(OnExitGame);
        _restartButton.onClick.AddListener(OnRestartGame);
    }

    private void OnDisable()
    {
        _exitButton.onClick.RemoveListener(OnExitGame);
        _restartButton.onClick.RemoveListener(OnRestartGame);
    }
    private void OnExitGame()
    {
        onExitGame?.Invoke(this, EventArgs.Empty);
    }

    private void OnRestartGame()
    {
        onRestartGame?.Invoke(this, EventArgs.Empty);
    }
 }
