using System;
using UnityEngine;

public class GameOverMenuController : MonoBehaviour
{
    private GameOverMenuModel _gameOVerModel;
    private GameOverMenuView _gameMenuView;

    private void Awake()
    {
        _gameOVerModel = GetComponent<GameOverMenuModel>();
        _gameMenuView = GetComponent<GameOverMenuView>();
    }

    private void OnEnable()
    {
        _gameMenuView.onExitGame += ExitGame;
        _gameMenuView.onRestartGame += RestartGame;
    }

    private void OnDisable()
    {
        _gameMenuView.onExitGame -= ExitGame;
        _gameMenuView.onRestartGame -= RestartGame;
    }

    private void RestartGame(object sender, EventArgs e)
    {
        _gameOVerModel.GameManagerActive=true;
        Time.timeScale = 1;
        gameObject.SetActive(false);        
    }
    private void ExitGame(object sender,EventArgs e)
    {
        Application.Quit();
    }
}
