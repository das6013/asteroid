using Player;
using UnityEngine;

public class GameOverMenuModel : MonoBehaviour
{
    [SerializeField] private GameManagerModel _gameManager;

    public bool GameManagerActive
    {
        get=> _gameManager.gameObject.activeSelf;
        set 
        {
            _gameManager.gameObject.SetActive(value);
        }
    }
}
