using System;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    private MainMenuView _mainMenuView;
    private MainMenuModel _mainMenuModel;

    private void Awake()
    {
        Time.timeScale = 0;
        _mainMenuView = GetComponent<MainMenuView>();
        _mainMenuModel= GetComponent<MainMenuModel>();
    }

    private void OnEnable()
    {
        _mainMenuView.onStartGame+=StartGame;
        _mainMenuView.onExitGame += ExitGame;
        _mainMenuView.onSettingGame += OpenSettingMenu;
        _mainMenuView.onContiousGame += ContiniosGame;
    }

    private void OnDisable()
    {
        _mainMenuView.onStartGame -= StartGame;
        _mainMenuView.onExitGame -= ExitGame;
        _mainMenuView.onSettingGame -= OpenSettingMenu;
        _mainMenuView.onContiousGame -= ContiniosGame;
        _mainMenuModel.SettingMenuOpen = false;
    }

    private void StartGame(object sender,EventArgs e)
    {
        _mainMenuModel.Game = false;
        _mainMenuModel.Game=true;
        Time.timeScale = 1;
        _mainMenuView.SetButtonSetting(true);
        gameObject.SetActive(false);

    }

    private void ExitGame(object sender, EventArgs e)
    {
        Application.Quit();
    }

    private void OpenSettingMenu(object sender, EventArgs e)
    {
        _mainMenuModel.SettingMenuOpen = true;
    }

    private void ContiniosGame(object sender, EventArgs e)
    {
        _mainMenuView.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
