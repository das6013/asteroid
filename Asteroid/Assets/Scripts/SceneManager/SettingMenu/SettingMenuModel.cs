using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class SettingMenuModel : MonoBehaviour
{
    private int _typeInput;
    public  EventHandler onChangeInput;
    public int TypeInput { get => _typeInput; set => _typeInput = value; }

    
    private void Awake()
    {
        _typeInput = PlayerPrefs.GetInt("InputSystem", 0);
        PlayerPrefs.SetInt("InputSystem", _typeInput);
        onChangeInput?.Invoke(this,EventArgs.Empty);
    }   

    private void OnDisable()
    {        
        PlayerPrefs.SetInt("InputSystem", _typeInput);
    }
}
