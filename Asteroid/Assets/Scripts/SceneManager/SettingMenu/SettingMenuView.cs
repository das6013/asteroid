using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingMenuView : MonoBehaviour
{
    [Header("Setting menu button")]
    [SerializeField] private Button _keyboardButton;
    [SerializeField] private Button _mouseButton;
    [SerializeField] private Button _exitButton;

    public event EventHandler onKeyaboardButtonClick;
    public event EventHandler onMouseuttonClick;
    public event EventHandler onExitMenu;
    
    private void OnEnable()
    {
        _keyboardButton.onClick.AddListener(OnKeyaboardButtonClick);
        _mouseButton.onClick.AddListener(OnMousedButtonClick);
        _exitButton.onClick.AddListener(OnExirButtonClick);
    }

    private void OnDisable()
    {
        _keyboardButton.onClick.RemoveListener(OnKeyaboardButtonClick);
        _mouseButton.onClick.RemoveListener(OnMousedButtonClick);
        _exitButton.onClick.RemoveListener(OnExirButtonClick);
    }

    private void ChangeButton(int id)
    {        
        if (id == 0)
        {
            _keyboardButton.gameObject.SetActive(false);
            _mouseButton.gameObject.SetActive(true);
        }
        else
        {
            _mouseButton.gameObject.SetActive(false);
            _keyboardButton.gameObject.SetActive(true);
        }
    }

    public void SetButton(int id)
    {
        if (id == 0)
        {
            _keyboardButton.gameObject.SetActive(true);
            _mouseButton.gameObject.SetActive(false);
        }
        else
        {
            _mouseButton.gameObject.SetActive(true);
            _keyboardButton.gameObject.SetActive(false);
        }
    }
    private void OnKeyaboardButtonClick()
    {
        onKeyaboardButtonClick?.Invoke(this,EventArgs.Empty);
        ChangeButton(0);
    }

    public void OnMousedButtonClick()
    {
        onMouseuttonClick?.Invoke(this, EventArgs.Empty);
        ChangeButton(1);
    }

    public void OnExirButtonClick()
    {
        onExitMenu?.Invoke(this, EventArgs.Empty);
    }
}
