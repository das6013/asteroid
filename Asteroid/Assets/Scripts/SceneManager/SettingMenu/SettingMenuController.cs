using System;
using UnityEngine;

public class SettingMenuController : MonoBehaviour
{
    private SettingMenuModel _settingMenu;
    private SettingMenuView _menuView;

    private void Awake()
    {
        _settingMenu= GetComponent<SettingMenuModel>();
        _menuView= GetComponent<SettingMenuView>();
    }

    private void OnEnable()
    {
        _menuView.onExitMenu += CloseSettingMenu;
        _menuView.onKeyaboardButtonClick += SetKeyboardInput;
        _menuView.onMouseuttonClick+= SetMouseInput;
        _menuView.SetButton(PlayerPrefs.GetInt("InputSystem",0));
    }

    private void OnDisable()
    {
        _menuView.onExitMenu -= CloseSettingMenu;
        _menuView.onKeyaboardButtonClick -= SetKeyboardInput;
        _menuView.onMouseuttonClick -= SetMouseInput;
    }

    private void CloseSettingMenu(object sender,EventArgs e)
    {
        gameObject.SetActive(false);
    }

    private void SetKeyboardInput(object sender,EventArgs e)
    {
        _settingMenu.TypeInput = 1;
        PlayerPrefs.SetInt("InputSystem", 1);
        _settingMenu.onChangeInput?.Invoke(this, EventArgs.Empty);
    }

    private void SetMouseInput(object sender, EventArgs e)
    {
        _settingMenu.TypeInput = 0;
        PlayerPrefs.SetInt("InputSystem", 0);
        _settingMenu.onChangeInput?.Invoke(this, EventArgs.Empty);
    }
}
