using Player;
using UnityEngine;

public class MainMenuModel : MonoBehaviour
{
    [SerializeField] private SettingMenuModel _settingMenu;
    [SerializeField] private GameManagerModel _game;
    public bool SettingMenuOpen
    {
        get => _settingMenu.gameObject.activeSelf;
        set=>_settingMenu.gameObject.SetActive(value);
    }
    public bool Game
    {
        get
        { 
            _game.isGameStart= true;
            return _game.gameObject.activeSelf;
        }
        set
        {
            _game.isGameStart = value;
            _game.gameObject.SetActive(value);
        }
    }
}
