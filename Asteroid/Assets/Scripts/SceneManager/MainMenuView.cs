using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    [Header("MainMenuButton")]
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _settingButton;
    [SerializeField] private Button _continiousButton;

    public event EventHandler onStartGame;
    public event EventHandler onExitGame;
    public event EventHandler onSettingGame;
    public event EventHandler onContiousGame;
    
    private void OnEnable()
    {
        _startButton.onClick.AddListener(OnStartGame);
        _exitButton.onClick.AddListener(OnExitGame);
        _settingButton.onClick.AddListener(OnSettingGame);
        _continiousButton.onClick.AddListener(OnContisiousGame);
    }

    public void SetButtonSetting(bool interact)
    { 
        _continiousButton.interactable= interact;
    }

    private void OnDisable()
    {
        _startButton.onClick.RemoveListener(OnStartGame);
        _exitButton.onClick.RemoveListener(OnExitGame);
        _settingButton.onClick.RemoveListener(OnSettingGame);
        _continiousButton.onClick.RemoveListener(OnContisiousGame);
    }

    private void OnStartGame()
    {
        onStartGame?.Invoke(this, EventArgs.Empty);
    }

    private void OnExitGame()
    {
        onExitGame?.Invoke(this, EventArgs.Empty);
    }

    private void OnSettingGame()
    {
        onSettingGame?.Invoke(this, EventArgs.Empty);
    }

    private void OnContisiousGame()
    {
        onContiousGame?.Invoke(this, EventArgs.Empty);
    }
}
