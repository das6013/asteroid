using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnerAsteroid : MonoBehaviour
{    
    [Header("AsteroidPrefab")]
    [SerializeField] private Asteroid _bigAsteroidPrefab;
    [SerializeField] private Asteroid _mediumAsteroidPrefab;
    [SerializeField] private Asteroid _smallAsteroidPrefab;

    [Header("SettingSpawner")]
    [SerializeField] private int _countMaxAsteroids;
    private int _currentCount;

    [Header("ScreenURL")]
    [SerializeField] private Canvas _canvas;

    [Header("AsteroidPool")]
    private AsteroidPool _bigAsteroids;
    private AsteroidPool _mediumAsteroids;
    private AsteroidPool _smallAsteroids;

    private Coroutine _spawnCoroutine;

    private void Awake()
    {
        _smallAsteroids = new AsteroidPool(_smallAsteroidPrefab, _countMaxAsteroids, null);
        _mediumAsteroids = new AsteroidPool(_mediumAsteroidPrefab, _countMaxAsteroids, _smallAsteroids.Get);
        _bigAsteroids = new AsteroidPool(_bigAsteroidPrefab, _countMaxAsteroids, _mediumAsteroids.Get);             
    }

    private void OnEnable()
    {
        _currentCount = _countMaxAsteroids;
        AsteroidPool.onConutChange += CheckAsteroid;
        SpawnASteroid();
    }

    private void OnDisable()
    {
        if (_spawnCoroutine != null)
        {
            StopCoroutine(_spawnCoroutine);
        }
        AsteroidPool.onConutChange-= CheckAsteroid;
        _smallAsteroids.ReturnAll();
        _mediumAsteroids.ReturnAll();
        _bigAsteroids.ReturnAll();        
    } 

    private Vector3 SetRandomPoint()
    {
        Vector3 point= Vector3.zero;
        float width = Screen.width/(_canvas.planeDistance)*0.5f;
        float height = Screen.height/(_canvas.planeDistance)*0.5f;
        var rand = (Random.Range(0, 4));
        if (rand == 0)
        {
            point.x = Random.Range(-width, width);
            point.y = height;
        }
        else if (rand == 1)
        {
            point.x = width;
            point.y = Random.Range(-height, height);
        }
        else if (rand == 2)
        {
            point.x = Random.Range(-width, width);
            point.y = -height;
        }
        else
        {
            point.x = -width;
            point.y = Random.Range(-height, height);
        }
        return point;
    }

    private void SpawnASteroid()
    {
        for (int i = 0; i <_currentCount; i++)
        {
            var astr=_bigAsteroids.Get();
            astr.StartMove(astr);
            astr.transform.position= SetRandomPoint();
        }
    }  

    private void CheckAsteroid(object sender,EventArgs e)
    {       
        if (_smallAsteroids.countActiveObject == 0 && _mediumAsteroids.countActiveObject == 0 && _bigAsteroids.countActiveObject == 0)
        {
            _currentCount++;
            _spawnCoroutine=StartCoroutine(SpawnBigAsteroid(3));
        }
    }
    IEnumerator SpawnBigAsteroid(int delay)
    {
        yield return new WaitForSeconds(delay);
        SpawnASteroid();
    }  
}
