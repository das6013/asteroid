public class SmallAsteroid : Asteroid
{
    public override void InitStrategy()
    {
        _stategyAsteroid = new SmallAsteroidStrategy(this);
    }
}
