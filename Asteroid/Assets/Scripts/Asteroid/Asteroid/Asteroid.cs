using System;
using Player;
using UnityEngine;


[RequireComponent(typeof(Rigidbody), typeof(Wrapper))]
public class Asteroid : MonoBehaviour, IScoreGet
{
    [Header("AsteroidMovementSetting")]
    [SerializeField] private float _asteroidSpeed;
    [SerializeField] private Vector3 _asteroidDirection;
    private Rigidbody _asteroidRigidbody;
    [Header("AsteroidGameLogicSetting")]    
    [SerializeField] protected int _score;

    public event EventHandler<Asteroid> onAsteroidDestroy;
    protected AsteroidStrategy _stategyAsteroid;
    private AsteroidPool _asteroidPool;
   
    public Vector3 AsteroidDirection { get => _asteroidDirection; set => _asteroidDirection = value; }
    public float AsteroidSpeed { get => _asteroidSpeed; set => _asteroidSpeed = value; }

    public GameManagerModel GameManagerModel => GameObject.FindAnyObjectByType<GameManagerModel>();

    public int Score { get => _score; set => _score=value; }

   

    private void OnEnable()
    {
        ResetRigidbody();
    }

    private void ResetRigidbody()
    {
        _asteroidRigidbody.velocity = Vector3.zero;
        _asteroidRigidbody.ResetCenterOfMass();
        _asteroidRigidbody.ResetInertiaTensor();
    }
    public virtual void InitStrategy()
    {
        
    }

    public void SetAsteroidPool(AsteroidPool asteroidPool)
    {
        _asteroidPool = asteroidPool;
    }   

    private void Awake()
    {
        InitStrategy();
        _asteroidRigidbody = GetComponent<Rigidbody>();     
    }

    public void StartMove(Asteroid asteroid)
    {
        _stategyAsteroid.StartMovement(asteroid);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Player.PlayerController>(out var player))
        {
            player.TakeDamege();
            _asteroidPool.Return(this);
        }
        else if (collision.gameObject.TryGetComponent<Bullet>(out var bullet))
        {
            bullet.DestroyBullet();
            onAsteroidDestroy?.Invoke(this, this);
            GetScore();
        }
        else if (collision.gameObject.TryGetComponent<UFOModel>(out var UFO))
        {
            UFO.gameObject.SetActive(false);
            _asteroidPool.Return(this);
        }
    }

    private void Update()
    {
        _stategyAsteroid.Move();
    }

    public void GetScore()
    {
        if(GameManagerModel!=null)
            GameManagerModel.ChangeScore(Score);
    }
}
