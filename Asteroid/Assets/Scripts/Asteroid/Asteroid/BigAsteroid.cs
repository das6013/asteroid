public class BigAsteroid : Asteroid
{
    public override void InitStrategy()
    {
        _stategyAsteroid = new BigAsteroidStrategy(this);
    }
}
