public class MediumAstreroid : Asteroid
{
    public override void InitStrategy()
    {
        _stategyAsteroid = new MediumAsteroidStrategy(this);
    }
}
