using UnityEngine;

public abstract class AsteroidStrategy
{
    protected Asteroid _asteroid;    

    protected AsteroidStrategy(Asteroid asteroid)
    {
        _asteroid = asteroid;
    }
   
    public abstract void StartMovement(Asteroid asteroid);    

    public void Move() 
    {
        _asteroid.transform.position += _asteroid.AsteroidDirection * Time.deltaTime * _asteroid.AsteroidSpeed;
    }

}
