using UnityEngine;

public class SmallAsteroidStrategy : AsteroidStrategy
{
    public SmallAsteroidStrategy(Asteroid asteroid) : base(asteroid)
    {
    }    

    public override void StartMovement(Asteroid asteroid)
    {
        var ofset = Random.Range(-.5f, .5f);
        var newDirection = new Vector3(asteroid.AsteroidDirection.x + ofset, asteroid.AsteroidDirection.y + ofset, 0).normalized;
        _asteroid.AsteroidDirection = newDirection;
        _asteroid.transform.position = asteroid.transform.position;
    }   
}
