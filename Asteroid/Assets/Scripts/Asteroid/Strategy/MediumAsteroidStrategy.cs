using UnityEngine;

public class MediumAsteroidStrategy : AsteroidStrategy
{
    public MediumAsteroidStrategy(Asteroid asteroid) : base(asteroid)
    {    }

    public override void StartMovement(Asteroid direction)
    {        
        var ofset = Random.Range(-.5f, .5f);       
        var newDirection = new Vector3(direction.AsteroidDirection.x + ofset, direction.AsteroidDirection.y + ofset, 0).normalized;
        _asteroid.AsteroidDirection = newDirection;
        _asteroid.transform.position=direction.transform.position;
    }
}
