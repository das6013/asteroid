using UnityEngine;
public class BigAsteroidStrategy : AsteroidStrategy
{
    public BigAsteroidStrategy(Asteroid asteroid) : base(asteroid)
    {
    } 
  
    public override void StartMovement(Asteroid direction)
    {
        direction.AsteroidSpeed = Random.Range(0.2f, direction.AsteroidSpeed);     
        var directionR = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0);
        _asteroid.AsteroidDirection= directionR.normalized;
    }
}
