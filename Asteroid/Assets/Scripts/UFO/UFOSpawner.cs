using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class UFOSpawner : MonoBehaviour
{
    [Header("UFO prefab")]
    [SerializeField] private UFOModel _ufoModel;

    [Header("Setting spawn")]
    [SerializeField] private float _minTimeSpawn;
    [SerializeField] private float _maxTimeSpawn;

    [Header("Link Screen")]
    [SerializeField] private Canvas _canvas;

    private float _spawmTime;
    private float _currentTime;

    private void Awake()
    {
        _spawmTime = Random.Range(_minTimeSpawn, _maxTimeSpawn);
    }    
   
    private void SpawnUFO()
    {             
        _ufoModel.transform.position=SetRandomPoint();
        _ufoModel.gameObject.SetActive(true);        
    }

    private Vector3 SetRandomPoint()
    {
        Vector3 point = Vector3.zero;
        float width = Screen.width / (_canvas.planeDistance)*0.5f;
        float height = Screen.height / (_canvas.planeDistance)*0.5f;
        var rand = (Random.Range(0, 4));
        if (rand == 0)
        {
            point.x = Random.Range(-width, width);
            point.y = 0.8f*height;
        }
        else if (rand == 1)
        {
            point.x = width;
            point.y = Random.Range(-height, height);
        }
        else if (rand == 2)
        {
            point.x = Random.Range(-width, width);
            point.y = -0.8f*height;
        }
        else
        {
            point.x = -width;
            point.y = Random.Range(-height, height);
        }
        return point;
    }

    private void Update()
    {
        SpawnLogic();
    }

    private void SpawnLogic()
    {
        if (_spawmTime > _currentTime)
        {
            _currentTime += Time.deltaTime;
        }
        else
        {
            SpawnUFO();
            _spawmTime = Random.Range(_minTimeSpawn, _maxTimeSpawn);
            _currentTime = 0;
        }
    }

    private void OnEnable()
    {   
        _ufoModel.onDestroyUFO += StopSpawn;
    }

    private void OnDisable()
    {
        StopSpawn(this, EventArgs.Empty);
        _ufoModel.onDestroyUFO -= StopSpawn;     
    }

    private void StopSpawn(object sender,EventArgs e)
    {
        _spawmTime = Random.Range(_minTimeSpawn, _maxTimeSpawn);
        _currentTime = 0;
    }

    
}
