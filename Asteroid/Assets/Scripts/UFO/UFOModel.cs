using System;
using ObjectPool;
using Player;
using UnityEngine;
using Random = UnityEngine.Random;

public class UFOModel : MonoBehaviour, IScoreGet
{
    [Header("UFO stat")]
    private float _speed;
    [SerializeField] private int _score = 200;
    [SerializeField] private float _lifeTimeUFO;
    private Vector3 _direction;

    [Header("Bullet setting")]
    [SerializeField] private Bullet _bulletPrefab;
    [SerializeField] private float _fireRate;
    [SerializeField] private float _delayNextShoot;
    private PoolBase<Bullet> _bulletPool;

    [Header("Screen setting")]
    [SerializeField] private float _planeDistance;

    public EventHandler onDestroyUFO;
  
    public float PlaneDistance { get => _planeDistance; set => _planeDistance = value; }
    public float Speed { get => _speed; set => _speed = value; }

    public float FireRate { get => _fireRate; set => _fireRate = value; }

    public Vector3 Direction { get => _direction; set => _direction = value; }

    public PoolBase<Bullet> BulletPool { get => _bulletPool;}

    public float DelayShoot { get => _delayNextShoot; }

    public float LifeTimeUFO { get => _lifeTimeUFO;}

    public GameManagerModel GameManagerModel => FindAnyObjectByType<GameManagerModel>();

    public int Score { get => _score; set => _score=value; }

    private void Awake()
    {
        _bulletPool = new PoolBase<Bullet>(Preload, GetAction, SetAction,1);
    }

    public Bullet Preload()
    {
        var bullet = Instantiate(_bulletPrefab, transform.position, transform.rotation);
        return bullet;
    }

    private void GetAction(Bullet bullet)
    {
        if (bullet != null)
        {
            bullet.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            bullet.transform.position = transform.position;
            bullet.gameObject.SetActive(true);
        }
    }

    private void SetAction(Bullet bullet)
    {
        if(bullet!=null)
            bullet.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        onDestroyUFO?.Invoke(this, EventArgs.Empty);
        _bulletPool.ReturnAll();
    }

    public void GetScore()
    {
        GameManagerModel.ChangeScore(_score);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Bullet>(out var bullet))
        {
            bullet.DestroyBullet();
            GetScore();
            gameObject.SetActive(false);
        }
    }
}
