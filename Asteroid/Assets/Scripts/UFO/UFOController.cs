using System;
using System.Collections.Generic;
using UnityEngine;

public class UFOController : MonoBehaviour
{
    private UFOModel _ufoModel;

    private float _changeTimeMovement=5f;
    private float _currentTime;
    private float _lifeTimeCheck;
    private List<Action> _typeMovement;
    private Action _currentMove;
    private int _id;

    public int ID
    {
        get => _id;
        set
        {
            if (value+1 > _typeMovement.Count)
            {
                _id = 0;
            }
            else
            {
                _id = value;
            }
        }
    }
 
    
    private void Awake()
    {
        _ufoModel = GetComponent<UFOModel>();
        _typeMovement = new List<Action>()
        {
            MoveForward,
            MoveSide
        };
        _currentMove = _typeMovement[0];
        _ufoModel.Speed = Screen.width / (_ufoModel.PlaneDistance *_ufoModel.LifeTimeUFO);
    }

    
    private void MoveForward()
    {
        transform.position+=new Vector3(_ufoModel.Direction.x,0,0)*Time.deltaTime*_ufoModel.Speed;
    }

    private void MoveSide()
    {
        transform.position += _ufoModel.Direction * Time.deltaTime * _ufoModel.Speed;
    }

    private void SetDirection()
    {
        var x = UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
        var y = UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
        _ufoModel.Direction=new Vector3(x,y,0);
    }

    private void Update()
    {
        MoveLogic();
        ShootLogic();
        LifeCicleLogic();
    }

    private void ShootLogic()
    {
        if (Time.time > _ufoModel.FireRate)
        {
            _ufoModel.FireRate = Time.time + _ufoModel.DelayShoot;
            Shoot();
        }
    }

    private void MoveLogic()
    {
        if (_currentTime < _changeTimeMovement)
        {
            _currentTime += Time.deltaTime;
        }
        else
        {
            ID++;
            _currentMove = _typeMovement[ID];
            _currentTime = 0;
        }
        if (_currentMove != null)
        {
            _currentMove();
        }
    }
    private void Shoot()
    {
        var bullet = _ufoModel.BulletPool.Get();
        bullet.OnBulletDestroy = () => { _ufoModel.BulletPool.Return(bullet); };
    }

    private void LifeCicleLogic()
    {
        if (_lifeTimeCheck < _ufoModel.LifeTimeUFO)
        {
            _lifeTimeCheck += Time.deltaTime;
        }
        else
        {
            _lifeTimeCheck = 0;
            gameObject.SetActive(false);
        }
    }
    
    private void OnEnable()
    {
        SetDirection();
        _currentMove = _typeMovement[0];
    }

    private void OnDisable()
    {
        ID = 0;
        _currentTime= 0;
        _lifeTimeCheck = 0;
    }    
}
