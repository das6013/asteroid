using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using TMPro;
using UnityEngine;

public class GameManagerView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _score;    
    private void Awake()
    {              
        ChangeScore(this, 0);
    }

    public void ChangeScore(object sender,int  score)
    {
        _score.text = $"SCORE:{score}";
    }

}
