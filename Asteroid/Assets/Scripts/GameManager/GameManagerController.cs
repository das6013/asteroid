using Player;
using UnityEngine;

public class GameManagerController : MonoBehaviour
{
    private GameManagerModel _gameManager;
    private GameManagerView _managerView;

    private void Awake()
    {
        _gameManager=GetComponent<GameManagerModel>();
        _managerView= GetComponent<GameManagerView>();
    }

    private void OnEnable()
    {
        _gameManager.onScoreChange += _managerView.ChangeScore;        
    }

    private void OnDisable()
    {
        _gameManager.onScoreChange -= _managerView.ChangeScore;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape)&&_gameManager.isGameStart)
        {
            _gameManager.SettingMenuActive=!_gameManager.SettingMenuActive;
            Time.timeScale = _gameManager.SettingMenuActive ? 0f : 1f;
        }
    }
}
