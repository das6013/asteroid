using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class GameManagerModel : MonoBehaviour
    {
        [SerializeField] private SettingMenuModel _settingMenu;
        [SerializeField] private GameObject _menu;
        [SerializeField] private Player _player;
        [SerializeField] private int _score;
        [SerializeField] private List<InputPlayerSystem> _playerSystems;
        [SerializeField] private GameOverMenuModel _gameOverMenu;
        
        [HideInInspector] public bool isGameStart=false;
        
        public int Score { get => _score;}

        public bool SettingMenuActive
        {
            get => _menu.activeSelf;set=> _menu.SetActive(value);
        }
        private void Awake()
        {
            _playerSystems = new List<InputPlayerSystem>()
            {
                new InputPlayerSystemKeyboard(_player),
                new InputPlayerSystemMouse(_player)
            };
            _player.PlayerConroller = _playerSystems[PlayerPrefs.GetInt("InputSystem",0)];
        }

        private void OnEnable()
        {
            isGameStart=true;
            _settingMenu.onChangeInput += ChangeInputSystem;
            _player.onPlayerDeath += OpenGameOverMenu;
            onScoreChange?.Invoke(this, _score);

        }

        private void OnDisable()
        {
            _settingMenu.onChangeInput -= ChangeInputSystem;
            _player.onPlayerDeath-= OpenGameOverMenu;
            _score = 0;
            onScoreChange?.Invoke(this,_score);
        }

        public event EventHandler<int> onScoreChange;

        public void ChangeScore(int score)
        {
            _score += score;
            onScoreChange?.Invoke(this, _score);
        }

        private void ChangeInputSystem(object sender,EventArgs e)
        {
            _player.PlayerConroller = _playerSystems[PlayerPrefs.GetInt("InputSystem", 0)];
        }

        private void OpenGameOverMenu(object sender,EventArgs e)
        {
            isGameStart = false;
            Time.timeScale = 0;
            _gameOverMenu.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
     }
}