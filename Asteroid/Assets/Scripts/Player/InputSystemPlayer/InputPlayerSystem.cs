using ObjectPool;
using UnityEngine;

namespace Player
{
    public abstract class InputPlayerSystem
    {
        protected Player _player;
        protected bool thruting;
        protected float _directionSpace;
        protected float fireDelay;

        public InputPlayerSystem(Player player)
        {
            _player = player;
        }        
        

        public abstract void SetMoveLogic();

        public virtual void Move()
        {
            if (thruting)
            {
                _player.PlayerRigidbody.AddForce(_player.transform.up * _player.SpaceMovementSpeed * Time.fixedDeltaTime, ForceMode.Impulse);
            }


            if (_directionSpace != 0f)
            {
                _player.transform.Rotate(Vector3.forward, _player.SpaceTurningSpeed * _directionSpace * Time.fixedDeltaTime);
            }
        }

        protected void Shoot()
        {
            var bullet = _player.BulletPool.Get();
            bullet.OnBulletDestroy = () => { _player.BulletPool.Return(bullet); };
        }
    }
}