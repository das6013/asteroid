
using Player;
using UnityEngine;

public class InputPlayerSystemKeyboard : InputPlayerSystem
{
    public InputPlayerSystemKeyboard(Player.Player player) : base(player)
    {
    }   

    public override void SetMoveLogic()
    {
        if(Input.GetKey(KeyCode.W)|| Input.GetKey(KeyCode.UpArrow))
            thruting= true;
        else
            thruting= false;

        if (Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.LeftArrow))
        {
            _directionSpace = 1f;
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            _directionSpace = -1f;
        }
        else
        {
            _directionSpace = 0f;
        }

        if (Input.GetKeyUp(KeyCode.Space) && Time.time > fireDelay)
        {
            fireDelay = Time.time + 1 / (float)_player.MaxCountBullet;
            Shoot();
        }
    }
}
