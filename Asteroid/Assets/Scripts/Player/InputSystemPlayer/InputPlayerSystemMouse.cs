using UnityEngine;
using Player;


public class InputPlayerSystemMouse : InputPlayerSystem
{
    private Vector3 _pointCursor;
 
    private float Speed
    {
        get {
            if (_player.SpaceTurningSpeed / 180f > 3)
            {
               return 3;
            }
            else
            {
                return _player.SpaceTurningSpeed / 180f;
            }
        }       
    }
    public InputPlayerSystemMouse(Player.Player player) : base(player)
    {
    }

    public override void SetMoveLogic()
    {        
        var pointCursor=Input.mousePosition;
        _pointCursor.x -= Screen.width / 2;
        _pointCursor.y -= Screen.height / 2;      
        
        var direction = (_player.transform.position.normalized - _pointCursor).normalized;
        
        if (Input.GetMouseButton(1))
            thruting = true;
        else
            thruting = false;        

        if (Input.GetMouseButtonDown(0) && Time.time > fireDelay)
        {
            fireDelay = Time.time + 1 / (float)_player.MaxCountBullet;
            Shoot();
        }

        if (_pointCursor != pointCursor)
        {
            Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, -direction);            
            _player.transform.rotation = Quaternion.Lerp(_player.transform.rotation, targetRotation, Time.deltaTime * Speed);
            _pointCursor = pointCursor;
        }
      
    }

    public override void Move()
    {
        if (thruting)
        {
            _player.PlayerRigidbody.AddForce(_player.transform.up * _player.SpaceMovementSpeed * Time.fixedDeltaTime, ForceMode.Impulse);
        }
        
    } 
}
