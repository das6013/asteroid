using System;
using System.Collections;
using ObjectPool;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody),typeof(Wrapper),typeof(BoxCollider))]
    public class Player : MonoBehaviour
    {
        [Header("PlayerStatSetting")]
        [SerializeField] private int _maxHealth;
        private int _currentHealth;

        [Header("PlayerMovementSetting")]
        [SerializeField] private float _spaceMovementSpeed = 0;
        [SerializeField] private float _spaceTurningSpeed = 0;

        [Header("BulletSetting")]
        [SerializeField] private Bullet _bulletPrefab;
        [SerializeField] private int _maxCountBullet = 3;
        private PoolBase<Bullet> _bulletPool;

        private InputPlayerSystem _playerConroller;
        private BoxCollider _boxCollider;
        private Rigidbody _playerRigidbody;
        private Coroutine _unbreakCorrutine;
        
        public event EventHandler<int> onHealthChange;
        public event EventHandler onPlayerDeath;

        public int Health
        {
            get => _currentHealth;
            set
            {
                if (0 >= value)
                {
                    _currentHealth = 0;
                }
                else
                {
                    _currentHealth = value;                    
                }
            }
        }
        public Rigidbody PlayerRigidbody { get => _playerRigidbody; }

        public float SpaceMovementSpeed { get => _spaceMovementSpeed; set => _spaceMovementSpeed = value; }

        public float SpaceTurningSpeed { get => _spaceTurningSpeed; set => _spaceTurningSpeed = value; }
        
        public int MaxCountBullet { get => _maxCountBullet;} 

        public InputPlayerSystem PlayerConroller { get => _playerConroller; set => _playerConroller = value; }

        public PoolBase<Bullet> BulletPool { get => _bulletPool;}        

        private void Awake()
        {
            _playerRigidbody = GetComponent<Rigidbody>();
            _boxCollider= GetComponent<BoxCollider>();
            _bulletPool = new PoolBase<Bullet>(Preload, GetAction, SetAction, _maxCountBullet);            
        }

        private void OnEnable()
        {
            _currentHealth = _maxHealth;
            _unbreakCorrutine=StartCoroutine(GetUnbrake());
            onHealthChange += CheckHealth;
        }

        private void OnDisable()
        {
            StopCoroutine(_unbreakCorrutine);
            _bulletPool.ReturnAll();
            _currentHealth = _maxHealth;
            onHealthChange -= CheckHealth;
            onHealthChange.Invoke(this, _currentHealth);            
        }

        public Bullet Preload()
        {
            var bullet = Instantiate(_bulletPrefab, transform.position, transform.rotation);
            return bullet;
        }


        private void GetAction(Bullet bullet)
        {            
            bullet.transform.rotation = transform.rotation;
            bullet.transform.position = transform.position;
            bullet.gameObject.SetActive(true);
        }
        private void SetAction(Bullet bullet)
        {
            if (bullet != null)
            {
                bullet.gameObject.SetActive(false);
            }            
            
        }

        private void ResetStat()
        {
            _playerRigidbody.ResetCenterOfMass();
            _playerRigidbody.ResetInertiaTensor();
            _playerRigidbody.velocity = Vector3.zero;
            transform.position = Vector3.zero;         
        }

        public void GetUnbreakPlayer()
        {
            _unbreakCorrutine = StartCoroutine(GetUnbrake());
        }

        private IEnumerator GetUnbrake()
        {            
            ResetStat();
            onHealthChange?.Invoke(this, _currentHealth);
            _boxCollider.enabled = false;
            yield return new WaitForSeconds(3f);
            _boxCollider.enabled = true;
        }

        private void CheckHealth(object sender,int health)
        {
            if (health < 1)
            {
                onPlayerDeath.Invoke(this, EventArgs.Empty);
            }
        }
    }
}