using System;
using TMPro;
using UnityEngine;
namespace Player
{
    public class PlayerView : MonoBehaviour
    {
        [Header("Player UI")]
        [SerializeField] private TextMeshProUGUI _healthText;

        [Header("Player Animation")]
        [SerializeField] private Animation _animation;
        
        public string HealthText
        {
            get => _healthText.text;
            set => _healthText.text = value;
        }      

        private void OnEnable()
        {
            
            _animation.Play();
        }

      
        private void SetHealth(int health)
        {
            HealthText = $"X{health}";
        }

        public void ChangeHealth(object sender, int health)
        {
            _animation.Play();
            SetHealth(health);
        }
        
    }
}