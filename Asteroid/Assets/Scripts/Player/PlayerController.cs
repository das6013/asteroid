using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        private Player _player;
        private PlayerView _playerView;

        private void Awake()
        {
            _player = GetComponent<Player>();     
            _playerView = GetComponent<PlayerView>();
        }

        private void OnEnable()
        {
            _player.onHealthChange += _playerView.ChangeHealth;
        }

        private void OnDisable()
        {
            _player.onHealthChange -= _playerView.ChangeHealth;
        }

        private void Update()
        {
            SetLogicMove();
        }

        private void FixedUpdate()
        {
            _player.PlayerConroller.Move();
        }

        private void SetLogicMove()
        {
            _player.PlayerConroller.SetMoveLogic();
        }               

        

        public void TakeDamege()
        {
            _player.Health--;
            _player.GetUnbreakPlayer();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.TryGetComponent<Bullet>(out var bullet))
            {
                bullet.DestroyBullet();
                TakeDamege();
                collision.gameObject.SetActive(false);
            }
            else if (collision.gameObject.TryGetComponent<UFOModel>(out var UFO))
            {
                TakeDamege();
                UFO.gameObject.SetActive(false);
            }

        }
    }
}