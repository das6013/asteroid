using System;
using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Header("BulletSetting")]
    [SerializeField] private int _speedBullet;
    [SerializeField] private float _planeDistance;

    private Vector3 startPoint;
   [SerializeField] private float _distance;
   [SerializeField] private float _currentDistance;
    private Vector3 _checkDistanse;

    public  Action OnBulletDestroy;

    public float PlaneDistance { get => _planeDistance; set => _planeDistance = value; }

    private void Start()
    {
        Debug.Log(_planeDistance);
        _distance = Screen.width/ _planeDistance;
    }

    private void OnEnable()
    {
        startPoint=transform.position;
        _checkDistanse = startPoint;
    }

    private void Update()
    {
        _currentDistance += Vector3.Distance(_checkDistanse, startPoint);
        if ( _currentDistance< _distance)
        {
            startPoint=transform.position;
            transform.position += transform.up*_speedBullet*Time.deltaTime;
            _checkDistanse = transform.position;                       
        }
        else
        {
            _currentDistance = 0;
            DestroyBullet();             
        }
    }    

    public void DestroyBullet()
    {
        OnBulletDestroy();
        _currentDistance = 0;
    }      
}
