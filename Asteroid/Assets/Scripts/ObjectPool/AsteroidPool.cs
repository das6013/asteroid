using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectPool;
using System;
using Random = UnityEngine.Random;

public class AsteroidPool : PoolBase<Asteroid>
{
    public static event EventHandler onConutChange;
    private Func<Asteroid> _spawnLogic;
    public AsteroidPool(Asteroid prefab, int countPreload, Func<Asteroid> spawnLogic) : base(() => Preload(prefab), GetAction, ReturnAction, countPreload)
    {
        _spawnLogic = spawnLogic;
    }
    public static Asteroid Preload(Asteroid prefab) => InitAsteroid(prefab);

    private static Asteroid InitAsteroid(Asteroid asteroid)
    {
        var ast = GameObject.Instantiate(asteroid);
        return ast;
    }

    public static void ReturnPool(Asteroid astr)
    {   
        if(astr!=null)
            astr.gameObject.SetActive(false);
    }
    public static void GetAction(Asteroid asteroid)=>asteroid.gameObject.SetActive(true);

    public static void ReturnAction(Asteroid asteroid) => ReturnPool(asteroid);

    public override void Return(Asteroid item)
    {
        base.Return(item);
        item.onAsteroidDestroy -= ReturnAsteroid;
        item.onAsteroidDestroy -= SpawnAsteroid;
        onConutChange?.Invoke(this, EventArgs.Empty);
    }   

    public void ReturnAsteroid(object sender, Asteroid asteroid)
    {
        asteroid.onAsteroidDestroy -= ReturnAsteroid;
        asteroid.onAsteroidDestroy-= SpawnAsteroid;
        Return(asteroid);        
    }

    public void SpawnAsteroid(object sender, Asteroid asteroid)
    {
        var astSpeed= Random.Range(0.2f, asteroid.AsteroidSpeed);
        if (_spawnLogic != null)
        {
            for (int i = 0; i < 2; i++)
            {
                var astr = _spawnLogic();
                astr.StartMove(asteroid);
                astr.AsteroidSpeed = astSpeed;
            }
        }
    }

    public override Asteroid Get()
    {
        Asteroid item = _pool.Count > 0 ? _pool.Dequeue() : _preloadFunc();
        _getAction(item);
        _activeItem.Add(item);        
        item.onAsteroidDestroy += SpawnAsteroid;
        item.onAsteroidDestroy += ReturnAsteroid;
        item.SetAsteroidPool(this);        
        return item;
    }
}
