using System;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPool
{
    public class PoolBase<TValue>
    {
        protected readonly Func<TValue> _preloadFunc;
        protected readonly Action<TValue> _getAction;
        protected readonly Action<TValue> _returnAction;
        protected Queue<TValue> _pool=new Queue<TValue>();
        protected List<TValue> _activeItem=new List<TValue>();
        public int countActiveObject
        { get { return _activeItem.Count; } }

        public PoolBase(Func<TValue> preloadFunc,Action<TValue> getAction,Action<TValue> returnAction, int preloadCount)
        {
            _preloadFunc= preloadFunc;
            _getAction= getAction;
            _returnAction = returnAction;
            if (preloadFunc == null)
            {
                Debug.LogError("Func null");
            }

            for (int i = 0; i < preloadCount; i++)
            {
                Return(preloadFunc());
            }
        }

        public virtual TValue Get()
        {
            TValue item = _pool.Count > 0 ? _pool.Dequeue() : _preloadFunc();
            _getAction(item);
            _activeItem.Add(item);
            return item;
        }

        public virtual void Return(TValue item)
        {
            _returnAction(item);
            _pool.Enqueue(item);
            _activeItem.Remove(item);
        }

        public virtual void ReturnAll()
        {
            for (int i=0; i < countActiveObject; i++)
            {
                _returnAction(_activeItem[i]);
                _pool.Enqueue(_activeItem[i]);
            }
            for (int i = 0; i < countActiveObject; i++)
            {
                _activeItem.Remove(_activeItem[i]);
            }
        }
    }
       
}
